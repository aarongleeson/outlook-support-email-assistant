VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisOutlookSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True

Sub Application_ItemSend(ByVal Item As Object, Cancel As Boolean)
 Dim objRecipSup As Recipient
 Dim objRecipSer As Recipient
 Dim objRecipSue As Recipient
 Dim strMsg As String
 Dim res As Integer
 Dim rec As Recipient
  Dim i As Long
   Dim Recipient As Outlook.Recipient
   Dim Recipients As Outlook.Recipients
    
   StrCCSup = "support@ebisolutions.co.uk"
 StrCCSer = "servicedesk@ebisupport.co.uk"
 StrSue = "sue.wilson@ebisolutions.co.uk"
 'StrSue = "aaron.gleeson@ebisolutions.co.uk"'
  Dim recips As Outlook.Recipients
    Dim recip As Outlook.Recipient
    Dim pa As Outlook.PropertyAccessor
    Dim prompt As String
  Const PR_SMTP_ADDRESS As String = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E"


 
 
 'checks if it is a email that is being sent, if  it is then it will perform the below checks'
 
 If TypeName(Item) = "MailItem" Then
 

 'Check if support or service desk are in the to address, if they are it removes it.'
 
 If Get_Hidden_Settings("RemoveTo") = 1 And (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
      For i = Item.Recipients.Count To 1 Step -1
       If Item.Recipients(i) Like "*EBI Support*" Then
       Item.Recipients.Remove (i)
        End If
         Next i
End If
     
If Get_Hidden_Settings("RemoveTo") = 1 And (Item.SentOnBehalfOfName Like "*EBI Service Desk*" Or Item.SentOnBehalfOfName Like "*EBI Support*") Then
    For i = Item.Recipients.Count To 1 Step -1
     If Item.Recipients(i) Like "*EBI Service Desk*" Then
        Item.Recipients.Remove (i)
     End If
    Next i
End If
 
 ' checks the settings to see if user has set to auto add CC if true and the from is set to support or service desk it will auto include the recipients'
 
 
If Get_Hidden_Settings("AddCC") = 1 And Item.SentOnBehalfOfName Like "*EBI Support*" Then
If Not (Item.CC Like "*EBI Support*" Or Item.CC Like "*EBI Service Desk*") Then
  Set objRecipSup = Item.Recipients.Add(StrCCSup)
  objRecipSup.Type = olCC
  End If
End If

If Get_Hidden_Settings("AddCC") = 1 And Item.SentOnBehalfOfName Like "*EBI Service Desk*" Then
If Not (Item.CC Like "*EBI Support*" Or Item.CC Like "*EBI Service Desk*") Then
  Set objRecipSer = Item.Recipients.Add(StrCCSer)
  objRecipSer.Type = olCC
  End If
End If

'Checks if you are sending from personal account or production'

If Not (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
 prompt$ = "You are not sending this from Support or Service Desk. Are you sure you want to send it?"
     If MsgBox(prompt$, vbYesNo + vbQuestion + vbMsgBoxSetForeground, "Check Sending Account") = vbNo Then
       Cancel = True
     End If

  End If
  
        
  ' Auto Replace Sue Osbourne with Sue Wilson '
    If Get_Hidden_Settings("RemoveSue") = 1 Then
    If Not (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
      For i = Item.Recipients.Count To 1 Step -1
       If Item.Recipients(i) Like "*Osborne, Sue*" Then
             Item.Recipients.Remove (i)
        Set objRecipSue = Item.Recipients.Add(StrSue)
      
         End If
         Next i
End If
End If

If Not (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
   Set recips = Item.Recipients
    For Each recip In recips
        Set pa = recip.PropertyAccessor
        If InStr(LCase(pa.GetProperty(PR_SMTP_ADDRESS)), "@ebisolutions.co.uk") = 0 Then
            strMsg = strMsg & "   " & pa.GetProperty(PR_SMTP_ADDRESS) & vbNewLine
        End If
    Next

    If strMsg <> "" Then
        prompt = "This email will be sent outside of EBI to:" & vbNewLine & strMsg & "Do you want to proceed?"
        If MsgBox(prompt, vbYesNo + vbExclamation + vbMsgBoxSetForeground, "Check Address") = vbNo Then
            Cancel = True
        End If
    End If
    
    End If
  
   ' REMOVED - Check for Sue Osbourne Email address - REMOVED '
    ' If Not (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
'      For i = Item.Recipients.Count To 1 Step -1
'       If Item.Recipients(i) Like "*Osborne, Sue*" Then
 '      prompt$ = "You have included Sue Osbourne!!! Do you want to continue?"
  '   If MsgBox(prompt$, vbYesNo + vbQuestion + vbMsgBoxSetForeground, "Check Sue Osbourne") = vbNo Then
   '    Item.Recipients.Remove (i)
    '       Cancel = True
     '   End If
      '   End If
       '  Next i
'End If

  
  'checks if the To - Sue Osbourne resolves correctly - added due to resolve error that was appearing'
  If Get_Hidden_Settings("RemoveSue") = 1 Then
  If Not (Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*") Then
  If Not Item.Recipients.ResolveAll Then
 
 For Each Recipient In Recipients
 
 If Not Recipients.Resolved Then
 
 MsgBox Recipient.Name
 
 End If
 
 Next
 
 End If
  End If
  End If
  
  
  'checks if the CC resolves correctly - added due to resolve error that was appearing'
  If Get_Hidden_Settings("AddCC") = 1 And Item.SentOnBehalfOfName Like "*EBI Support*" Then
  If Not (Item.CC Like "*EBI Support*" Or Item.CC Like "*EBI Service Desk*") Then
  If Not objRecipSup.Resolve Then
    strMsg = "Could not resolve the cc recipient. " & _
    "Do you want still to send the message?"
    res = MsgBox(strMsg, vbYesNo + vbDefaultButton1, _
   "Could Not Resolve cc Recipient")
    If res = vbNo Then
    Cancel = True
    End If
    End If
  End If
  End If
  
  If Get_Hidden_Settings("AddCC") = 1 And Item.SentOnBehalfOfName Like "*EBI Service Desk*" Then
  If Not (Item.CC Like "*EBI Support*" Or Item.CC Like "*EBI Service Desk*") Then
   If Not objRecipSer.Resolve Then
    strMsg = "Could not resolve the cc recipient. " & _
    "Do you want still to send the message?"
    res = MsgBox(strMsg, vbYesNo + vbDefaultButton1, _
   "Could Not Resolve cc Recipient")
    If res = vbNo Then
    Cancel = True
    End If
    End If
  End If
  End If
  
   ' To check if the sender is EBI Support or EBI Service Desk to ensure support or service desk is CC'd in'
If Item.SentOnBehalfOfName Like "*EBI Support*" Or Item.SentOnBehalfOfName Like "*EBI Service Desk*" Then
If Not (Item.CC Like "*EBI Support*" Or Item.CC Like "*EBI Service Desk*") Then
       prompt$ = "You have not included support or service desk in CC. Are you sure you want to send it?"
     If MsgBox(prompt$, vbYesNo + vbQuestion + vbMsgBoxSetForeground, "Check CC Account") = vbNo Then
       Cancel = True
  End If
  End If
  End If


End If
 End Sub


