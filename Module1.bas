Attribute VB_Name = "Module1"

Public Sub YesToCC()
CC_Settings_Hidden_yes

End Sub

Public Function Hidden_Settings_CC() As Boolean
'Checks to see if the settings exist already'
Dim oNs As Outlook.NameSpace
Dim oFL As Outlook.Folder
Dim oItem As Outlook.StorageItem

On Error GoTo OL_Error


Set oNs = Application.GetNamespace("MAPI")
Set oFld = oNs.GetDefaultFolder(olFolderInbox)
Set oItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)

If oItem.Size <> 0 Then
    Hidden_Settings_CC = True
    Else
    Hidden_Settings_CC = False
End If

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear

End Function


Public Function CC_Settings_Hidden_yes()
'Sets the AddCC value to 1 if user wants CC to auto happen'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)

oSItem.UserProperties.Add "AddCC", olText
oSItem.UserProperties("AddCC").Value = 1

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function

Public Function Get_Hidden_Settings(AddCC) As String
'This calls the settings'
Dim oNs As Outlook.NameSpace
Dim oFL As Outlook.Folder
Dim oItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oNs = Application.GetNamespace("MAPI")
Set oFld = oNs.GetDefaultFolder(olFolderInbox)
Set oItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)

If oItem.Size <> 0 Then
    Get_Hidden_Settings = oItem.UserProperties(AddCC)
End If

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear

End Function


Public Sub NoToCC()
CC_Settings_Hidden_No
CC_Settings_Hidden_RemoveTo_No
CC_Settings_Hidden_RemoveSue_No

End Sub

Public Function CC_Settings_Hidden_No()
'Sets the AddCC value to 0 if the user chooses not to have Auto CCing active'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)

oSItem.UserProperties.Add "AddCC", olText
oSItem.UserProperties("AddCC").Value = 0

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function

Public Function CC_Settings_Hidden_RemoveTo_Yes()
'Sets the RemoveTo Value to 1 if the user chooses to auto remove EBI from the To line'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)

oSItem.UserProperties.Add "RemoveTo", olText
oSItem.UserProperties("RemoveTo").Value = 1

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function
Public Function CC_Settings_Hidden_RemoveTo_No()
'Sets the RemoveTo Value to 0 if the user chooses not to auto remove EBI from the to line'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)


oSItem.UserProperties.Add "RemoveTo", olText
oSItem.UserProperties("RemoveTo").Value = 0

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function
Public Function CC_Settings_Hidden_RemoveSue_No()
'Sets the RemoveSue Value to 0 if the user chooses not to auto remove Sue Osbourne from the to line'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)


oSItem.UserProperties.Add "RemoveSue", olText
oSItem.UserProperties("RemoveSue").Value = 0

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function
Public Function CC_Settings_Hidden_RemoveSue_Yes()
'Sets the RemoveSue Value to 1 if the user chooses not to auto remove Sue Osbourne from the to line'
Dim oNs As Outlook.NameSpace
Dim oFld As Outlook.Folder
Dim oSItem As Outlook.StorageItem

On Error GoTo OL_Error

Set oFld = Application.Session.GetDefaultFolder(olFolderInbox)
Set oSItem = oFld.GetStorage("Hidden Settings", olIdentifyBySubject)


oSItem.UserProperties.Add "RemoveSue", olText
oSItem.UserProperties("RemoveSue").Value = 1

oSItem.Save

Exit Function
OL_Error:
MsgBox (Err.Description)
Err.Clear



End Function
Sub OpenSettings()
SettingsForm.Show

End Sub


