VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} SettingsForm 
   Caption         =   "Settings"
   ClientHeight    =   2685
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   3435
   OleObjectBlob   =   "SettingsForm.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "SettingsForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub SaveBtn_Click()
If YNBox.Value = True Then
Call CC_Settings_Hidden_yes

SettingsForm.Hide
Else
Call CC_Settings_Hidden_No

SettingsForm.Hide
End If

If ToBox.Value = True Then
Call CC_Settings_Hidden_RemoveTo_Yes

SettingsForm.Hide
Else
Call CC_Settings_Hidden_RemoveTo_No

SettingsForm.Hide
End If

If SueBx.Value = True Then
Call CC_Settings_Hidden_RemoveSue_Yes

SettingsForm.Hide
Else
Call CC_Settings_Hidden_RemoveSue_No

SettingsForm.Hide
End If
End Sub


Private Sub UserForm_Initialize()

 Call Hidden_Settings_CC
If Hidden_Settings_CC = False Then
   Call CC_Settings_Hidden_No
   Call CC_Settings_Hidden_RemoveTo_No
   Call CC_Settings_Hidden_RemoveSue_No
End If

If Get_Hidden_Settings("AddCC") = 1 Then
YNBox.Value = True
Else
YNBox.Value = False
End If
If Get_Hidden_Settings("RemoveTo") = 1 Then
ToBox.Value = True
Else
ToBox.Value = False
End If
If Get_Hidden_Settings("RemoveSue") = 1 Then
SueBx.Value = True
Else
SueBx.Value = False
End If
End Sub
